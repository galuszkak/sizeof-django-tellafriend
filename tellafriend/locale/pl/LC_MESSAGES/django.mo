��          �      l      �  -   �               %     -     B  G   S  N   �  	   �  	   �     �               /     4     ;     I     V  
   Z  .   e  �  �  #   :     ^     l     |     �     �  3   �  I   �     *  	   9  	   C     M  	   Z     d     v     �     �     �     �  ,   �              	               
                                                                     %(sender_email)s recommends this page to you: Captcha Friend e-mail Message Message from sender: Message from you Please enter the characters shown in the image in the field next to it. Please enter your and the recipient's e-mail address, and an optional message. Recipient Recommend Recommendation Recommendation by Recommendations Send Sender Sender e-mail Sending date URL User agent Your recommendation was sent out successfully. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-03 23:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %(sender_email)s poleca Ci stronę: Kod z obrazka Email znajomego Wiadomość Wiadomość od nadawcy: Wiadomość Prosimy wprowadzić poprawne znaki w polu tekstowym Prosimy wprowadzić poprawny adres odbiorcy oraz opcjonalną wiadomość. Email odbiorcy Polecenie Polecenie Polecenie od Polecenia Wyślij polecenie Email nadawcy Email nadawcy Data wysłania URL Przeglądarka Twoje polecenie zostało wysłane poprawnie. 