Changelog
=========


v0.1.0 (2014-07-30)

    * Fix "Error: cannot import name get_user_model" (method is available for dj version >= 1.5)

v0.0.8 (2014-07-25)

    * Bump version

v0.0.7 (2014-07-24)

    * Small fix in template

v0.0.6 (2014-07-21)

    * Handle configurable user-model

v0.0.2 (2014-07-03)

    * Small fixes in email template.

v0.0.1 (2014-07-02)

    * Initial version
